'use strict';
const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

// Init backend
var path = require("path");
var php = require("gulp-connect-php");
php.server({
    port: 8088,
    base: path.resolve(__dirname) + '../DiscordBotApi', // Backend directory
    bin: path.resolve(__dirname) + "../bin/php" // Php bin
});

var mainWindow;

// Quit when all windows are closed.
app.on('window-all-closed', function() {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('ready', function() {
  mainWindow = new BrowserWindow({width: 900, height: 600});

  // and load the app's front controller. Feel free to change with app_dev.php
  mainWindow.loadURL("http://127.0.0.1:8088/app.php");

  // Uncomment to open the DevTools.
  //mainWindow.webContents.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
});
